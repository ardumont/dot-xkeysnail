# -*- coding: utf-8 -*-

from time import sleep
import re

from xkeysnail.transform import (
    Key, define_modmap, define_keymap, K, with_mark, set_mark,
    escape_next_key, launch, pass_through_key
)


# Firefox bindings
# https://support.mozilla.org/en-US/kb/keyboard-shortcuts-perform-firefox-tasks-quickly?redirectlocale=en-US&as=u&redirectslug=Keyboard+shortcuts&utm_source=inproduct


# [Global modemap] Change modifier keys as in xmodmap
define_modmap({
    Key.CAPSLOCK: Key.LEFT_CTRL,
    Key.RIGHT_CTRL: Key.LEFT_CTRL,
    Key.RIGHT_ALT: Key.LEFT_ALT,
})

# [Conditional modmap] Change modifier keys in certain applications
# define_conditional_modmap(re.compile(r'Emacs'), {
#     Key.RIGHT_CTRL: Key.ESC,
# })

# [Multipurpose modmap] Give a key two meanings. A normal key when pressed and
# released, and a modifier key when held down with another key. See Xcape,
# Carabiner and caps2esc for ideas and concept.
# define_multipurpose_modmap(
#     # Enter is enter when pressed and released. Control when held down.
#     {Key.ENTER: [Key.ENTER, Key.RIGHT_CTRL]},

#     # Capslock is escape when pressed and released. Control when held down.
#     # {Key.CAPSLOCK: [Key.ESC, Key.LEFT_CTRL]
#     # To use this example, you can't remap capslock with define_modmap.
# )


# Keybindings for Firefox/Chrome
define_keymap(re.compile("Firefox|Google-chrome"), {
    # search in text
    K("C-s"): K("C-f"),
    K("C-c"): {
        K("u"): K("C-f6"),       # go to url box
        K("s"): K("C-j"),        # go to search box
        K("r"): K("C-f5"),       # refresh page
        K("t"): K("C-Shift-t"),  # unclose tab
        K("d"): K("C-Shift-y"),  # go to downloads
        K("h"): K("C-h"),        # history side bar
        K("a"): K("C-Shift-a"),  # add-ons
        K("n"): K("C-t"),        # new tab
        # K("Slash"): K("M-C-t")   # undo close tab
    },
    # history
    K("Shift-b"): K("M-left"),   # back
    K("Shift-f"): K("M-right"),  # forward
    # switch tab
    K("M-n"): K("C-Tab"),
    K("M-p"): K("C-Shift-Tab"),
    # very naive "Edit in editor" feature (just an example)
    K("C-o"): [K("C-a"), K("C-c"), launch(
        ["gedit"]), sleep(0.5), K("C-v")],
}, "Firefox and Chrome")

# Keybindings for Zeal https://github.com/zealdocs/zeal/
# define_keymap(re.compile("Zeal"), {
#     # Ctrl+s to focus search area
#     K("C-s"): K("C-k"),
# }, "Zeal")

# Application who already have emacs bindings
WHITELIST_APPS = ("Emacs", "URxvt", "Conkeror", "qutebrowser")

# Emacs-like keybindings in non-Emacs applications
define_keymap(lambda wm_class: wm_class not in WHITELIST_APPS, {
    # basis
    K("C-i"): K("TAB"),
    K("C-M-i"): K("Shift-TAB"),
    K("C-m"): K("Enter"),
    # Cursor
    K("C-b"): with_mark(K("left")),
    K("C-f"): with_mark(K("right")),
    K("C-p"): with_mark(K("up")),
    K("C-n"): with_mark(K("down")),
    K("C-h"): with_mark(K("backspace")),
    # backward-kill-word
    K("M-C-h"): [
        set_mark(True),
        with_mark(K("C-left")),
        K("delete"),
        set_mark(False)
    ],
    # Forward/Backward word
    K("M-b"): with_mark(K("C-left")),
    K("M-f"): with_mark(K("C-right")),
    # Beginning/End of line
    K("C-a"): with_mark(K("home")),
    K("C-e"): with_mark(K("end")),
    # Page up/down
    K("M-v"): with_mark(K("page_up")),
    K("C-v"): with_mark(K("page_down")),
    # Beginning/End of file
    K("M-Shift-comma"): with_mark(K("C-home")),
    K("M-Shift-dot"): with_mark(K("C-end")),
    # Newline
    K("C-m"): K("enter"),
    K("C-j"): K("enter"),
    K("C-o"): [K("enter"), K("left")],
    # Copy
    K("C-w"): [K("C-x"), set_mark(False)],
    K("M-w"): [K("C-c"), set_mark(False)],
    K("C-y"): [K("C-v"), set_mark(False)],
    # Delete
    K("C-d"): [K("delete"), set_mark(False)],
    K("M-d"): [K("C-delete"), set_mark(False)],
    # Kill line
    K("C-k"): [K("Shift-end"), K("C-x"), set_mark(False)],
    # Undo
    K("C-Slash"): [K("C-z"), set_mark(False)],
    # Mark
    K("C-space"): [K("esc"), set_mark(True)],
    # Search
    K("C-s"): K("F3"),
    K("C-r"): K("Shift-F3"),
    K("M-Shift-key_5"): K("C-h"),
    # Cancel
    K("C-g"): [K("esc"), set_mark(False)],
    # Escape
    K("C-q"): escape_next_key,
    # C-x YYY
    K("C-x"): {
        # C-x h (select all)
        K("h"): [K("C-home"), K("C-a"), set_mark(True)],
        # C-x C-f (open)
        K("C-f"): K("C-o"),
        # C-x C-s (save)
        K("C-s"): K("C-s"),
        # C-x k (kill tab)
        K("k"): K("C-f4"),
        # C-x C-c (exit)
        K("C-c"): K("C-q"),
        # cancel
        K("C-g"): pass_through_key
    },
    # undo
    K("C-slash"): [K("C-z"), set_mark(False)],
    # passthrough
    K("C-Shift-f1"): pass_through_key,
    K("C-Shift-f2"): pass_through_key,
    K("C-Shift-f3"): pass_through_key,
    K("C-Shift-f4"): pass_through_key,
    K("C-Shift-f5"): pass_through_key,
    K("C-Shift-f6"): pass_through_key,
    K("C-Shift-f7"): pass_through_key,
    K("C-Shift-f8"): pass_through_key,
    K("C-Shift-f9"): pass_through_key,
    K("C-Shift-f10"): pass_through_key,
    K("C-Shift-f11"): pass_through_key,
    K("C-Shift-f12"): pass_through_key,
}, "Emacs-like keys")
